use std::{
    error::Error,
    path::{Path, PathBuf},
};

use rust_xlsxwriter::{ExcelDateTime, Format, Formula, Workbook};

/// Reads a CSV file from a path and converts it to an xlsx workbook.
///
/// It is assumed the path has been checked before it's passed.
///
/// The workbook contains 4 sheets:
/// - 1. `Header`: Contains the DAQ's header
/// - 2. `Data`: Contains all of the DAQ's recorded data
/// - 3. `Graphs`: Empty, used later to add graphs.
/// - 4. `Analysis`: Contains the Minimum, Maximum and Average of every column in the `Data` sheet.
///
/// # Errors
/// Will return an error in the following cases:
/// - A record is malformed
/// - A value in a record is malformed
/// - The workheet can not be written to
#[inline]
pub fn csv_to_xlsx(input_file: &PathBuf) -> Result<Workbook, Box<dyn Error>> {
    let mut csv_reader = csv::Reader::from_path(input_file)?;
    let mut xlsx_workbook = Workbook::new();

    let mut analysis_columns: Vec<String> = Vec::new();

    // Shadowing is necessary for 1 and 2 because they keep mutable references of xlsx_workbook.
    // 3 is not bound and 4 is the last existing reference, so those don't need the shadowing.
    // I decided to add it for those to make the code more consistent and clear.

    // Make and fill 1. `Header`
    {
        const HEADER_END: &str = "User Description";
        let mut found_data = false;

        let header_worksheet = xlsx_workbook.add_worksheet().set_name("Header")?;
        for (row, record) in csv_reader.records().enumerate() {
            if found_data {
                break;
            }
            for (col, value) in record?.iter().enumerate() {
                let value = String::from_utf8(strip_ansi_escapes::strip(value)?)
                    .expect("Error converting value in CSV record to String");
                if value == HEADER_END {
                    found_data = true;
                    break;
                }
                header_worksheet.write_string(row as u32, col as u16, &value)?;
            }
        }
        header_worksheet.autofit();
    }

    // Make and fill 2. `Data`
    {
        let number_format = Format::new().set_num_format("0.00");
        let date_format = Format::new().set_num_format("yyyy-mm-dd hh:mm:ss.000");

        let data_worksheet = xlsx_workbook.add_worksheet().set_name("Data")?;
        for (row, record) in csv_reader.records().enumerate() {
            let record = record?;
            for (col, value) in record.iter().enumerate() {
                let value = String::from_utf8(strip_ansi_escapes::strip(value)?)
                    .expect("Error converting value in CSV record to String");
                if row == 0 {
                    // If it's the first row, we don't want to do anything
                } else if row == 1 {
                    data_worksheet.write_string((row - 1) as u32, col as u16, &value)?;
                    analysis_columns.push(value);
                } else if col == 0 {
                    let date = ExcelDateTime::parse_from_str(&value)?;
                    data_worksheet.write_datetime(
                        (row - 1) as u32,
                        col as u16,
                        &date,
                        &date_format,
                    )?;
                } else {
                    let number: f64 = match value.trim().parse() {
                        Ok(number) => number,
                        Err(_) => break,
                    };
                    data_worksheet.write_number_with_format(
                        (row - 1) as u32,
                        col as u16,
                        number,
                        &number_format,
                    )?;
                }
            }
        }
        data_worksheet.autofit();
    }

    // Make 3. `Graphs`
    {
        let _ = xlsx_workbook.add_worksheet().set_name("Graphs")?;
    }

    // Make 4. `Analysis`
    {
        let analysis_worksheet = xlsx_workbook.add_worksheet().set_name("Analysis")?;

        let column_num = analysis_columns.len();
        for j in 0..=column_num {
            if j >= 1 && j <= column_num - 2 {
                analysis_worksheet.write_string(j as u32 * 2, 0, &analysis_columns[j + 1])?;
            }
        }
        for j in 0..=column_num {
            if j == 0 {
                analysis_worksheet.write_string(0, 1, "Minimum")?;
            } else {
                let col = num_to_col(j);
                let formula = Formula::new(&format!("=MIN(Data!{col}:{col})\n"));
                analysis_worksheet.write_formula(j as u32 * 2, 1, formula)?;
            }
        }
        for j in 0..=column_num {
            if j == 0 {
                analysis_worksheet.write_string(0, 2, "Maximum")?;
            } else {
                let col = num_to_col(j);
                let formula = Formula::new(&format!("=MAX(Data!{col}:{col})\n"));
                analysis_worksheet.write_formula(j as u32 * 2, 2, formula)?;
            }
        }
        for j in 0..=column_num {
            if j == 0 {
                analysis_worksheet.write_string(0, 3, "Average")?;
            } else {
                let col = num_to_col(j);
                let formula = Formula::new(&format!("=AVERAGE(Data!{col}:{col})\n"));
                analysis_worksheet.write_formula(j as u32 * 2, 3, formula)?;
            }
        }
        analysis_worksheet.autofit();
    }

    Ok(xlsx_workbook)
}

/// Converts a number to spreadsheet column letters
fn num_to_col(number: usize) -> String {
    let mut result = String::new();
    let mut num = number + 2;

    while num > 0 {
        let rem = (num - 1) % 26;
        let ch = (rem as u8 + b'A') as char;
        result.insert(0, ch);
        num = (num - 1) / 26;
    }

    result
}

/// Checks if a path is situable to be read as a CSV file
///
/// # Errors
/// Will return an error if the path doesn't point to a file or if the file is not a CSV.
pub fn check_path(path: &Path, needed_extenstion: &str) -> Result<PathBuf, String> {
    const BAD_TYPE: &str = "The selected file is not the correct type.";
    const NOT_FILE: &str = "The selected elemet is not a file.";

    if !path.is_file() {
        return Err(String::from(NOT_FILE));
    }

    let extension = match path.extension() {
        Some(extension) => extension,
        None => return Err(String::from(BAD_TYPE)),
    };

    if extension == needed_extenstion {
        Ok(path.to_path_buf())
    } else {
        Err(String::from(BAD_TYPE))
    }
}

#[cfg(test)]
mod parse_convert {
    use std::path::PathBuf;
    use temp_file::TempFileBuilder;

    use super::*;

    #[test]
    fn double_letter_column() {
        assert_eq!(num_to_col(39), "AO");
    }

    #[test]
    fn check_path_correct() {
        let t = TempFileBuilder::new().suffix(".csv").build().unwrap();
        let path = t.path().to_path_buf();
        let result = check_path(&path, "csv");

        assert_eq!(Ok(path), result);
    }

    #[test]
    fn check_path_bad_extension() {
        let t = TempFileBuilder::new().suffix("csv").build().unwrap();
        let path = t.path().to_path_buf();
        let result = check_path(&path, "xlsx");

        assert_eq!(
            Err(String::from("The selected file is not the correct type.")),
            result
        );
    }

    #[test]
    fn check_path_unexistent_file() {
        let path = PathBuf::from("/tmp/test.txt");
        let result = check_path(&path, "txt");

        assert_eq!(
            Err(String::from("The selected elemet is not a file.")),
            result
        );
    }
}
