//! A template file looks like this:
//! ```toml
//! [[graph]]
//! title = "Channels 1 and 2"
//! time_range = "Data!$A$2:$A$3000"
//! y_axis_name = "Temperature"
//! value_ranges = ["Data!$C$2:$C$3000", "Data!$D$2:$D$3000"]
//! value_ranges_names = ["Channel 1", "Channel 2"]
//!
//! [[graph]]
//! title = "Channels 3 and 4"
//! time_range = "Data!$A$2:$A$3000"
//! y_axis_name = "Temperature"
//! value_ranges = ["Data!$E$2:$E$3000", "Data!$F$2:$F$3000"]
//! value_ranges_names = ["Channel 3", "Channel 4"]
//! ```
//! It contains any number of `[[graph]]` tables that contain the following parameters:
//! - `title` The title of the graph
//! - `time_range` The time range (from what row to what row) that the graph should contain
//! - `value_ranges` An array containing all the ranges of data that should be drawn as lines on the graph.
//! - `value_ranges_names` An array containing the names of the `value_ranges` to be displayed at
//! the legend of the graph. The index of the name matches that of the range on the `value_ranges`
//! array

use std::{
    error::Error,
    fs::{read_to_string, File},
    io::Write,
    path::PathBuf,
};

use rust_xlsxwriter::{Chart, ChartType, Workbook, XlsxError};
use serde_derive::{Deserialize, Serialize};
use toml::to_string;

/// Represents the toml [[graph]] table
#[derive(Serialize, Deserialize, Clone)]
pub struct Graph {
    pub title: String,
    pub time_range: String,
    pub y_axis_name: String,
    pub value_ranges: Vec<String>,
    pub value_ranges_names: Vec<String>,
}

/// Represents the whole toml file as a list of tables
#[derive(Serialize, Deserialize)]
pub struct GraphingTemplate {
    graph: Option<Vec<Graph>>,
}

/// Error type returned by `read_template()`
pub enum TemplateParsing {
    Unreadable(String),
    Unparseable(String),
    Empty,
}

/// Reads the graphing template file to a list of tables
///
/// It is assumed the path is checked before it is passed to the function.
///
/// # Errors
/// Will return an error in the following cases:
/// - The file can't be read
/// - The TOML is malformed and can't be parsed
/// - The TOML file is empty
pub fn read_template(path: PathBuf) -> Result<Vec<Graph>, TemplateParsing> {
    let template: String = match read_to_string(path) {
        Ok(template) => template,
        Err(err) => return Err(TemplateParsing::Unreadable(err.to_string())),
    };

    let template: GraphingTemplate = match toml::from_str(&template) {
        Ok(template) => template,
        Err(err) => return Err(TemplateParsing::Unparseable(err.to_string())),
    };

    template.graph.ok_or(TemplateParsing::Empty)
}

/// Serializes the vector of graphs into a toml file and saves it to the provided path
///
/// It is assumed the path is checked before it is passed to the function.
///
/// # Errors
/// Will return an error in the following cases:
/// - The vector of graphs can't be serialized
/// - The path is incorrect
/// - The path cannot be written to
pub fn save_template(graphs: &[Graph], path: PathBuf) -> Result<(), Box<dyn Error>> {
    let template = to_string(&GraphingTemplate {
        graph: Some(graphs.to_vec()),
    })?;

    let mut template_file = File::create(path)?;
    template_file.write_all(template.as_bytes())?;
    Ok(())
}

/// Creates graphs from a list of graph tables
///
/// # Errors:
/// Will return an error in the following cases:
/// - The empty workseet for the graphs can't be accessed
/// - A graph cannot be inserted to the worksheet
#[allow(dead_code)]
pub fn add_graphs(workbook: &mut Workbook, template: &[Graph]) -> Result<(), XlsxError> {
    const GRAPHS_SHEET: usize = 2;

    let graphs_worksheet = workbook.worksheet_from_index(GRAPHS_SHEET)?;
    let mut graph_row = 0;
    for graph in template {
        let mut chart = Chart::new(ChartType::ScatterSmooth);
        chart.title().set_name(&graph.title);
        chart.x_axis().set_name("Time");
        chart.y_axis().set_name(&graph.y_axis_name);
        chart.set_width(800);
        chart.set_height(400);
        for (i, range) in graph.value_ranges.iter().enumerate() {
            chart
                .add_series()
                .set_values(range)
                .set_categories(&graph.time_range)
                .set_name(&graph.value_ranges_names[i]);
        }
        graphs_worksheet.insert_chart(graph_row, 0, &chart)?;
        graph_row += 20;
    }
    Ok(())
}
