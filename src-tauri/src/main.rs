#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::{path::PathBuf, ffi::OsString};

use parking_lot::Mutex;
use rust_xlsxwriter::Workbook;
use tauri::State;

mod parse_convert;
use parse_convert::{check_path, csv_to_xlsx};

mod graphing_template;
use graphing_template::{read_template, save_template, Graph, TemplateParsing};

#[derive(Default)]
struct AppState {
    workbook: Workbook,
    template: Vec<Graph>,
    state: States,
}

#[derive(PartialEq, Eq)]
enum States {
    CsvParse(Status),
    ApplyGraph(Status),
    Export(Status),
}

#[derive(PartialEq, Eq)]
enum Status {
    None,
    Success,
    Fail,
}

impl Default for States {
    fn default() -> Self {
        Self::CsvParse(Status::None)
    }
}

/// Executed when the first step icon is pressed.
///
/// Checks if the provided path is correct and parses the csv file into a Workbook. Returns the
/// path on success.
///
/// # Errors
/// Will return an error in the following cases:
/// - The path is not a readable CSV file
/// - The CSV can't be converted to a workbook
#[tauri::command]
async fn read_csv(state: State<'_, Mutex<AppState>>, path: String) -> Result<String, String> {
    const PARSE_FAIL: &str = "Could not parse the CSV file.";

    let path_buf = check_path(&PathBuf::from(&path), "csv")?;

    let mut state = state.lock();

    if let Ok(workbook) = csv_to_xlsx(&path_buf) {
        state.workbook = workbook;
        state.state = States::CsvParse(Status::Success);
        Ok(path)
    } else {
        state.state = States::CsvParse(Status::Fail);
        Err(String::from(PARSE_FAIL))
    }
}

// TODO: Implement graphing screen
//
// mod graphing_template;
// use graphing_template::{add_graphs, read_template};
//
// #[tauri::command]
// async fn apply_graph(state: State<'_, Mutex<AppState>>) -> Result<String, String> {
//     let mut state = state.lock();
//
//     add_graphs(&mut state.workbook, todo!()).unwrap();
//
//     todo!();
// }

/// Executed when the select templpate from file button is pressed
///
/// Checks if the provided path is correct and deserializes the file. Returns a status message on
/// failure or success.
///
/// # Errors
/// Will error in the following cases:
/// - The path is not a readable `.agt` file
/// - The toml can't be parsed
/// - The file is empty
#[tauri::command]
async fn import_template(
    state: State<'_, Mutex<AppState>>,
    path: String,
) -> Result<String, String> {
    const IMPORT_SUCCESS: &str = "Template loaded from file ✓";
    const UNREADABLE_FILE: &str = "Template couldn't be read. Try again.";
    const PARSE_ERROR: &str = "Template couldn't be read. Try again.";
    const FILE_EMPTY: &str = "This file doesn't contain any template.";

    let path_buf = check_path(&PathBuf::from(&path), "agt")?;

    let mut state = state.lock();

    match read_template(path_buf) {
        Ok(template) => {
            state.template = template;
            Ok(String::from(IMPORT_SUCCESS))
        }
        Err(err) => match err {
            TemplateParsing::Unreadable(_) => Err(String::from(UNREADABLE_FILE)),
            TemplateParsing::Unparseable(_) => Err(String::from(PARSE_ERROR)),
            TemplateParsing::Empty => Err(String::from(FILE_EMPTY)),
        },
    }
}

/// Executed when the save template to file button is pressed
///
/// Checks if the provided path is correct and returns a success or failure message
///
/// # Errors
/// Will return an error in the following cases:
/// - The path doesn't represent an `.agt` file
/// - The path can't be written to
/// - The vector of graphs can't be serialized
#[tauri::command]
async fn export_template(
    state: State<'_, Mutex<AppState>>,
    path: String,
) -> Result<String, String> {
    const EXPORT_SUCCESS: &str = "Template saved to file.";
    const EXPORT_FAIL: &str = "Could not export template to file";
    const BAD_PATH: &str = "Template export path is incorrect. Try again";

    let path_buf = PathBuf::from(&path);
    if path_buf.extension() != Some(&OsString::from("agt")) {
        return Err(String::from(BAD_PATH));
    }

    let template = &state.lock().template;

    match save_template(template, path_buf) {
        Ok(()) => Ok(String::from(EXPORT_SUCCESS)),
        Err(_) => Err(String::from(EXPORT_FAIL)),
    }
}

/// Executed when the third step icon is pressed.
///
/// Checks if the provided path is correct and exports the workbook to an xlsx file
/// If succeeded, returns the path the file has beens saved to. If failed, returns a status message
/// describing the error
#[tauri::command]
async fn export_xlsx(state: State<'_, Mutex<AppState>>, path: String) -> Result<String, String> {
    const EXPORTABLE_STATES: [States; 3] = [
        States::CsvParse(Status::Success),
        States::ApplyGraph(Status::Success),
        States::Export(Status::Success),
    ];

    const EXPORT_FAIL: &str = "Failed to export file.";
    const EXPORT_UNABLE: &str = "Can't export yet.";

    let path_buf = check_path(&PathBuf::from(&path), "xlsx")?;

    let mut state = state.lock();

    if EXPORTABLE_STATES.contains(&state.state) {
        state.workbook.read_only_recommended();

        if matches!(state.workbook.save(path_buf), Ok(())) {
            state.state = States::Export(Status::Success);
            return Ok(path);
        }

        state.state = States::Export(Status::Fail);
        return Err(String::from(EXPORT_FAIL));
    }

    Err(String::from(EXPORT_UNABLE))
}

fn main() {
    tauri::Builder::default()
        .manage(Mutex::new(AppState::default()))
        .invoke_handler(tauri::generate_handler![
            read_csv,
            export_xlsx,
            import_template,
            export_template
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
