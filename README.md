# DAQinator
Rust rewrite of [Keysight DAQ - Agilent Data Automation Program](https://gitlab.com/ColinLeftwich/AgilentDataAutomationProgram). Comes with major performance and reliability improvements. 

![homescreen](images/homescreen.png)

## TODO
- [X] Move DAQ header from CSV to first sheet in XLSX file
- [X] Move DAQ logged data from CSV to second sheet in XLSX file
- [X] Import graphing template file
- [X] Create graphs in third XLSX sheet from template file
- [X] Export graphing template
- [X] Do basic data analytics (min, max, avg)
- [ ] Add a graphing template creator
- [X] Make async
- [ ] Add a more visual progress feedback (spinner, progress bar...)
- [ ] Add significant number formatting based on column title

## Running
### Installing dependencies
`rust` and `pnpm` are needed to build the project. `just` is optional

Run `pnpm install` on the project root directory to install the rest of dependencies

### Developement build
Run `just run` or `cargo tauri dev` to run a developement build
